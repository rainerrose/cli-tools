# Verknüpfung: powershell.exe -noprofile -executionpolicy Bypass -file %userprofile%\get-logged-in-user.ps1
# quser = query user
(((quser) -replace '^>', '') -replace '\s{2,}', ',').Trim() | ForEach-Object {
    if ($_.Split(',').Count -eq 5) {
        Write-Output ($_ -replace '(^[^,]+)', '$1,')
    } else {
        Write-Output $_
    }
} | ConvertFrom-Csv | Out-GridView -PassThru 

#eof