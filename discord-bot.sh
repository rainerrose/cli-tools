#!/bin/bash
# Discord Webhook
THE_SCRIPTNAME="${1}"
discord_url="https://discord.com/api/webhooks/958845757975330887/wr21BPYs5jECw2OTAo86crAjGrAvbgaexvCAKqCFCgVDr5IFxQtrm5ON4CRAaBmP"

generate_post_data() {
  cat <<EOF
{
  "content": "Backup fertig",
  "embeds": [{
    "title": "Backup Script ist fertig!",
    "description": "Script ${1}",
    "color": "45973"
  }]
}
EOF
}

# POST request to Discord Webhook
curl -H "Content-Type: application/json" -X POST -d "$(generate_post_data ${THE_SCRIPTNAME} )" $discord_url
