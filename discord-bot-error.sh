#!/bin/bash
# Discord Webhook
THE_SCRIPTNAME="${1}"
FEHLER_MELDUNG="${2}"
discord_url="https://discord.com/api/webhooks/958845757975330887/wr21BPYs5jECw2OTAo86crAjGrAvbgaexvCAKqCFCgVDr5IFxQtrm5ON4CRAaBmP"

generate_post_data() {
  cat <<EOF
{
  "content": "Backup Error",
  "embeds": [{
    "title": "Backup Script wirft Fehler!",
    "description": "Script ${1}\nIrgendwas ist bei diesem Script schiefgelaufen.",
    "color": "15548997"
  }]
}
EOF
}

# POST request to Discord Webhook
curl -H "Content-Type: application/json" -X POST -d "$(generate_post_data ${THE_SCRIPTNAME} ${FEHLER_MELDUNG})" $discord_url
