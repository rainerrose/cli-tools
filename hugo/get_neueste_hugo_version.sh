#!/bin/bash

zeile=$(curl --silent "https://api.github.com/repos/gohugoio/hugo/releases/latest"  | jq -r .tag_name)
echo $zeile 
version=$(echo $zeile | sed -e 's/^v//')
echo "Version = '$version'"
wget "https://github.com/gohugoio/hugo/releases/download/v${version}/hugo_extended_${version}_Linux-64bit.tar.gz" -O hugo.tar.gz
tar xzf hugo.tar.gz hugo && rm hugo.tar.gz && printf "Erfolgreich runtergeladen.\n\nVersion: "
./hugo version
# eof