$HUGO_VERSION="0.92.1"
# Versions-Nummer muss noch händisch angepasst werden.
$URL="https://github.com/gohugoio/hugo/releases/download/v"+$HUGO_VERSION+"/hugo_extended_"+$HUGO_VERSION+"_Windows-64bit.zip"
Invoke-WebRequest -Uri $URL -OutFile hugo.zip
Expand-Archive hugo.zip -Force  -DestinationPath hugo-orig-win
Move-Item -Force hugo-orig-win\hugo.exe -Destination . 
Remove-Item hugo.zip
