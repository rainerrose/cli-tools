# Wozu dient dieses Repository?

Script-Sammlung bzw. -Vorlagen für Dinge, die ich immer mal wieder brauch und jetzt zentral ablege.

## bash-skel.sh

Basis für eigene Skripte, enthaelt bereits einige Skript-Standardelemente 
(usage-Funktion, Optionen parsen mittels getopts, vordefinierte Variablen etc.)
 
## discord-bot.sh

Setzt eine Discord-Nachricht per Webhook ab - Typ Info

## discord-bot-error.sh

Setzt eine Discord-Nachricht per Webhook ab - Typ Fehler

## docker/update_docker_images.sh

Durchpflügt die `docker-compose.yml`-Datei (parameterisierbar in der Datei) und fragt interaktiv nach, ob ein neues Image gepulled werden soll.

Wenn ja (default), dann legt das Script vorher ein Backup des Containers an, indem es an den Tag-Namen des aktuellen Containers ein `_backup` dran hängt.
Vorher wird das evtl. vorhanden und so benannte Image noch gelöscht, damit die Festplatte nicht platzt.
Der Tag-Name ist anpassbar im Script.

Die Ausgabe sieht dann z.B. so aus:

~~~
Update docker image: mysql:8 [j]|n: 
Lösche evtl. Backups
docker images mysql:8_backup
LINES_MIT_HEADER=2
lösche altes Image
docker image rm mysql:8_backup
Untagged: mysql:8_backup
Untagged: mysql@sha256:566007208a3f1cc8f9df6b767665b5c9b800fc4fb5f863d17aa1df362880ed04
Deleted: sha256:b2013ac9910129ded1da4c96495142b2ed0638ddf7e86e65156400d9a8503777
Deleted: sha256:d24171674816d2cdb9a7cfaf9980f3ba574930efe226ca7fa840084072974e08
Deleted: sha256:046ec448aa8eaaa27e4118211c9f77da24d35b8a84b25f997334c3473d14a870
Deleted: sha256:ab05f09658d83ed8ff487d346fcc9fa4cecf5a23906a7e7feffaea83a1332a14
Deleted: sha256:96db44e22a968a3a16890685d95bd96d420d99a8880966056721c36842fa38af
Deleted: sha256:402981938079002ce0b77d9fe7a892608155b3866ad1967313b4934d0d8a6d3c
Deleted: sha256:e38cf6d3efcf0edd5d80758556fdafa43e9e3136edbc05928febf8771b1cba31
Deleted: sha256:0d197db846f4e62ab3d29cef5d359f5896ddd6869ba49608f276741da328ef29
Deleted: sha256:121010cc4276929c2ba287fe57dfd0c61483862809dfc884c2aa3d998d6cc292
Deleted: sha256:b72678d2fc4485cc1febd27ab3cc6fe2ddc9ef62b2f016aaa8620040a339677f
Deleted: sha256:b42107e741524c1f305a1a4649a4d96a5921c48c5b8c438ad871ab7cc5815231
Lege Backup an 
docker tag mysql:8 mysql:8_backup
8: Pulling from library/mysql
210eb976c4c7: Pull complete 
ebb4c64fd647: Pull complete 
cb8397d67a0f: Pull complete 
ff0e9bc8aa4e: Pull complete 
e6f52b272e7f: Pull complete 
e2d9e8e437ee: Pull complete 
e61517738248: Pull complete 
cfb2700e7252: Pull complete 
4044404847dd: Pull complete 
c142db3aeff8: Pull complete 
Digest: sha256:1ee299bf9eb8d2218fcb4fad666a090c92caef48ce524e6edce35f2e2d55170d
Status: Downloaded newer image for mysql:8
docker.io/library/mysql:8
REPOSITORY   TAG        IMAGE ID       CREATED        SIZE
mysql        8_backup   8da80fe49fcf   4 weeks ago    577MB
mysql        8          2d9aad1b5856   2 months ago   574MB
~~~

## docker/devdir/docker-compose.yml

Das Unterverzeichnis `devdir` ist ein Verzeichnis um die Scripte zu testen.
Die dort enthaltene `docker-complete.yml` dient z.B. um das Script `docker/update_docker_images.sh` mal zu programmieren, weiterzuentwickeln und zu testen.

## docker/watch_docker_images

Scripte um Docker-Images zu monitoren auf Updates. Mehr Infos und Erläuterungen im Blog-Artikel: https://www.rainerrose.de/posts/check_last_docker_pull/

### docker/watch_docker_images/check_last_docker_pull.sh

Hier wird die Registry von docker abgefragt (hub.docker.com).

### docker/watch_docker_images/update_rc.sh

Script um die Konfigurationsdatei von `watch_docker_images/check_last_docker_pull.sh` leichter zu pflegen.

### docker/watch_docker_images/check_last_github_release.sh

Hier wird die Registry von github abgefragt.


## gen_passwort.sh

Erzeugt ein zufälliges Passwort aus default 64 Zeichen. 
Als Parameter kann die Anzahl der gewünschten Zeichen mitgegeben werden.
Wunderbar um in Scripten automatisiert Passwörter zu erzeugen.

## get-logged-in-user.ps1

Ein PowerShell-Script was die angemeldeten User unter Windows anzeigt.

Als Verknüpfung auf den Desktop legen. Spart das Aufrufen des Taskmanagers und gibt zudem mehr Infos.
Funktioniert sogar, wenn der Taskmanager gesperrt ist.

## Hugo

Hugo ist ein statischer Webseitengenerator; mehr Infos auf [gohugo.io](https://gohugo.io/).

Da Hugo über keine Auto-Update-Möglichkeit verfügt, habe ich mir Scripte geschrieben.


### hugo/get_neueste_hugo_version.sh

Das ausgereifteste Script. Saugt die `extended`-Version herunter, entpackt es und löscht das tar-Archiv wieder.

Voraussetzungen:

* `bash`
* `wget`
* `tar`
* `jq`
* `sed`


### hugo/update_hugo-binary.ps1

Die Versions-Nummer muss noch händisch angepasst werden. Mich da genauer mit der `JSON`-Antwort beim Download auseinanderzusetzen fehlte mir die Zeit und Lust.

Die jeweils gewünschte (meist aktuellste) Version auf [GitHub](https://github.com/gohugoio/hugo/releases) ansehen und die Versionsnummer im Script anpassen und Datei abspeichern.

Z.B `v0.92.1` zu
~~~ powershell
$HUGO_VERSION="0.92.1"
~~~
