#!/bin/bash
# Autor: Rainer Rose

DIRNAME_AUFRUF=$(dirname "${0}")
CONFIGFILE="${DIRNAME_AUFRUF}/$(basename $0 .sh).rc.json"
#DEBUG=on
RETURNCODE=0
QUIET=$1

jq . $CONFIGFILE  >/dev/null

if [ $? != 0 ]
then
	echo "Syntax-Fehler in der Konfig-Datei $CONFIGFILE"
	exit 3
fi

jq -r ".[] | .repro"  $CONFIGFILE | sort -u | while read -r REPRO
do
{
    test -z $DEBUG || echo "REPRO=$REPRO"
    jq -r --arg REPRO "${REPRO}" ".[] | select (.repro==\"$REPRO\")| .release"  "${CONFIGFILE}" | while read -r RELEASE
    do
      test -z $DEBUG || echo "RELEASE=$RELEASE"
      zeile=$(curl --silent "https://api.github.com/repos/${REPRO}/releases/latest" | jq -r .tag_name)
      test -z $DEBUG || echo "zeile=$zeile"
      VERSION=$(echo $zeile | perl -p -e 's/^v//')
      test -z $DEBUG || echo "Version = '$VERSION'"

      INFO=$(jq -r --arg REPRO "${REPRO}" ".[] | select (.repro==\"$REPRO\")| .info"  "${CONFIGFILE}")
              
      if [ "${RELEASE}" != "${VERSION}" ]
      then 
        echo "WARNING: Neues Release verfuegbar ${REPRO} alt: $RELEASE neue Version ${VERSION} Info: ${INFO}" >&2
        RETURNCODE=1
      else
        test "$QUIET" = "-q" || echo "OK: Release ${REPRO} Info: ${INFO}"
      fi
    done
}
done

exit $RETURNCODE