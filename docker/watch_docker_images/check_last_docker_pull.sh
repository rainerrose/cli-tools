#!/bin/bash
# Autor: Rainer Rose 

DIRNAME_AUFRUF=$(dirname "${0}")
CONFIGFILE="${DIRNAME_AUFRUF}/$(basename $0 .sh).rc.json"
#DEBUG=on
RETURNCODE=0
PARAMETER=$1
QUIET="n"

jq . $CONFIGFILE  >/dev/null

if [ $? != 0 ]
then
	echo "Syntax-Fehler in der Konfig-Datei $CONFIGFILE"
	exit 3
fi

test "${PARAMETER}" = "-u" && QUIET="-q"
test "${PARAMETER}" = "-q" && QUIET="-q"
TMPFILE=$(mktemp -t check_last_docker_pull.XXXXXXXXXXXXX)

jq -r ".[] | .imagename"  $CONFIGFILE | sort -u | while read -r IMAGENAME
do
{
    jq -r --arg IMAGENAME "${IMAGENAME}" ".[] | select (.imagename==\"$IMAGENAME\")| .tag"  "${CONFIGFILE}" | while read -r TAG
    do
      DATE=$(jq -r --arg IMAGENAME "${IMAGENAME}" --arg TAG "${TAG}" ".[] | select (.imagename==\"$IMAGENAME\" and .tag==\"$TAG\")| .date"  "${CONFIGFILE}")
      INFO=$(jq -r --arg IMAGENAME "${IMAGENAME}" --arg TAG "${TAG}" ".[] | select (.imagename==\"$IMAGENAME\" and .tag==\"$TAG\")| .info"  "${CONFIGFILE}")
      test -z $DEBUG || echo "DATE = $DATE IMAGENAME = $IMAGENAME TAG = $TAG"
      #URL="https://registry.hub.docker.com/v2/repositories/${IMAGENAME}/tags/?page_size=1000"
      #  URL="https://hub.docker.com/v2/repositories/library/tomcat/tags/9.0"
      URL="https://hub.docker.com/v2/repositories/${IMAGENAME}/tags/${TAG}"
      test -z $DEBUG || echo "URL = $URL"
      #LAST_PUSHED=$(curl -s "${URL}" | jq -r --arg TAG "${TAG}" ".results[] | select(.name == \"$TAG\") | {last_updated} | join(\" \")" )
      LAST_PUSHED=$(curl -s "${URL}" | jq -r "  {last_updated} | join(\" \")")
      test -z $DEBUG || echo "LAST_PUSHED = $LAST_PUSHED"
      if [ "${LAST_PUSHED}" != "${DATE}" ]
      then
          DATUM=$(date -d "${LAST_PUSHED}" '+%Y-%m-%d %H:%M')
          echo "WARNING: Neues Docker-Image verfuegbar ${IMAGENAME}:${TAG} seit $DATUM (\"${LAST_PUSHED}\") Info: ${INFO}" >&2
          RETURNCODE=1
          test "${PARAMETER}" = "-u" && echo ${DIRNAME_AUFRUF}/update_rc.sh "${IMAGENAME}:${TAG}" "${LAST_PUSHED} # Info: ${INFO} " >> "${TMPFILE}"
      else
        test "${QUIET}" = "-q" || echo "OK: Docker-Image ${IMAGENAME}:${TAG} Info: ${INFO}"
      fi
    done
}
done

test -e ${TMPFILE} && cat ${TMPFILE} && rm ${TMPFILE}
exit $RETURNCODE
