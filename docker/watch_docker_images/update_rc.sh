#!/bin/bash
# Autor: Rainer Rose
DIRNAME_AUFRUF=$(dirname "${0}")
CONFIGFILE="${DIRNAME_AUFRUF}/check_last_docker_pull.rc.json"
#DEBUG=on
RETURNCODE=0
IMAGENAME_AND_TAG="${1}"
DATUM="${2}"

test -z "${IMAGENAME_AND_TAG}" && echo "Image-Name und Tag angeben! bspw: library/mysql:8.0.28" >&2 && exit 2
test -z "${DATUM}" && echo "Image-Name und Tag und Datum angeben! bspw: library/mysql:8.0.28 '2022-09-13T04:12:41.047203Z'" >&2 && exit 2

jq . $CONFIGFILE  >/dev/null

if [ $? != 0 ]
then
	echo "Syntax-Fehler in der Konfig-Datei $CONFIGFILE"
	exit 3
fi

test -z "${IMAGENAME_AND_TAG}" && echo "aktualisiere Datum bei: ${IMAGENAME_AND_TAG}"
# bsp. library/mysql:8.0.28
IMAGENAME=$(echo "${IMAGENAME_AND_TAG}" | cut -f 1 -d ":")
TAG=$(echo "${IMAGENAME_AND_TAG}" | cut -f 2 -d ":")

test -z "${DEBUG}" || echo "DEBUG: IMAGENAME ${IMAGENAME} TAG ${TAG}"

jq --arg IMAGENAME "${IMAGENAME}" --arg TAG "${TAG}" --arg DATUM "${DATUM}" "(.[] | select (.imagename==\"$IMAGENAME\" and .tag==\"$TAG\") | .date )|= \"$DATUM\" "  $CONFIGFILE \
> "${CONFIGFILE}.neu"

diff -y --suppress-common-lines "${CONFIGFILE}.neu" "$CONFIGFILE"
mv -v "${CONFIGFILE}.neu" "$CONFIGFILE"
