#!/bin/bash
COMPOSE_FILE="docker-compose.yml"
BACKUP_TAG="_backup"

function usage {
         echo "Usage: $SCRIPTNAME [-h] [-v] [-b <Suffix-Tag Backup] [-c <docker-compose Datei>]" >&2
         [[ $# -eq 1 ]] && exit $1 || exit $EXIT_FAILURE 
}

while getopts ':b:c:vh' OPTION ; do
         case $OPTION in
         v)        VERBOSE=y
                 ;;
         b)        BACKUP_TAG="$OPTARG"
                 ;;                 
         c)        COMPOSE_FILE="$OPTARG"
                 ;;
         h)        usage $EXIT_SUCCESS
                 ;;
         \?)        echo "Unbekannte Option \"-$OPTARG\"." >&2
                 usage $EXIT_ERROR
                 ;;
         :)        echo "Option \"-$OPTARG\" benötigt ein Argument." >&2
                 usage $EXIT_ERROR
                 ;;
         *)        echo "Dies kann eigentlich gar nicht passiert sein..." >&2
                 usage $EXIT_BUG
                 ;;
         esac
done

for dimage in $(grep -E "\s+image:" $COMPOSE_FILE|cut -d ":" -f 2- | cut -d "#" -f 1 |xargs)
do
    echo -n "Update docker image: $dimage [j]|n: "
    read JANEIN
    # ggf bereinigen und docker.io/library/ rauswerfen
    dimage=$(echo "${dimage}" | sed 's/docker.io\/library\///')
    if [ "${JANEIN}" = "n" ]
    then
        echo "Skippe."
    else
        echo "Lösche evtl. Backups"
        echo "docker images ${dimage}${BACKUP_TAG}"
        LINES_MIT_HEADER=$(docker images "${dimage}${BACKUP_TAG}"| wc -l)
        # REPOSITORY   TAG             IMAGE ID       CREATED       SIZE
        # nginx        latest_backup   ac8efec875ce   5 weeks ago   142MB
        # 2 Zeilen = es gibt eins
        # 1 Zeile = leer, keines da, nur Uberschriften
	echo "LINES_MIT_HEADER=$LINES_MIT_HEADER"
        if [ "${LINES_MIT_HEADER}" -eq 2 ]
        then
            echo "lösche altes Image"
            echo "docker image rm ${dimage}${BACKUP_TAG}"
            docker image rm ${dimage}${BACKUP_TAG}
        else  
            echo "Kein Image mit Endung ${BACKUP_TAG} vorhanden."
        fi
        echo "Lege Backup an "
        echo docker tag "${dimage}" "${dimage}${BACKUP_TAG}"
        docker tag "${dimage}" "${dimage}${BACKUP_TAG}"
        docker pull "${dimage}"
		IMAGE_OHNE_TAG=$(echo "${dimage}"|cut -d ":" -f 1 )
        docker images "${IMAGE_OHNE_TAG}*"
    fi
done
