#!/usr/bin/bash
# Author: Rainer Rose
ZEICHENANZAHL=$1
DEFAULT_ZEICHENANZAHL=64

test -z $ZEICHENANZAHL && ZEICHENANZAHL=$DEFAULT_ZEICHENANZAHL
test "${ZEICHENANZAHL}" = "-h" && echo "usage: $0 [<Zeichenanzahl>]" && exit

tr -dc A-Za-z0-9 </dev/urandom | head -c "${ZEICHENANZAHL}"
echo "" # Leerzeile
